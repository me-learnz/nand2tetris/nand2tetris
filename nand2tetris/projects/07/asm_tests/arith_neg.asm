// Testing translations of arithmetic commands in CPU Emulator
//
// Stack before:      Stack after:
//  | x |              |  x |
//  | y |              | -y |
//  |   | <- SP        |    | <- SP

// Bootstrap code
@256
D=A
@SP
M=D

// x = 8
@8
D=A
@SP
A=M
M=D
@SP
M=M+1
// y = 7
@7
D=A
@SP
A=M
M=D
@SP
M=M+1

// neg
@SP
M=M-1
A=M
M=-M  // op
@SP
M=M+1