// Testing `push temp i` translation in CPU Emulator

// Bootstrap code
@256
D=A
@SP
M=D

// Save 7 to Mem[5+3]
@7
D=A
@8
M=D

// push temp 3 (put Mem[5+3] to stack)
@5
D=A
@3
A=D+A
D=M

@SP
A=M
M=D
@SP
M=M+1