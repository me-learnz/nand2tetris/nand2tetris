// Testing `push local i` translation in CPU Emulator

// Bootstrap code
@256
D=A
@SP
M=D

// Init LCL to 10 (example)
@10
D=A
@LCL
M=D

// Save 5 to Mem[Mem[LCL+1]]
@5
D=A
@LCL
A=M+1
M=D

// push local 1 (put Mem[Mem[LCL] + 1] to stack)
@LCL
D=M
@1
A=D+A
D=M

@SP
A=M
M=D
@SP
M=M+1