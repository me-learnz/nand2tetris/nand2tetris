// Testing `push static i` translation in CPU Emulator

// Bootstrap code
@256
D=A
@SP
M=D

// Save 7 to Mem[16] (1st variable)
@7
D=A
@16
M=D

// push static 3 (put variable `push_static.3` to stack)
@push_static.3
D=M

@SP
A=M
M=D
@SP
M=M+1