// Testing `pop temp i` translation in CPU Emulator

// Bootstrap code
@256
D=A
@SP
M=D

// Save 5 to stack
@5
D=A
@SP
A=M
M=D
@SP
M=M+1

// pop temp 3 (put value from stack to Mem[5+3])
@5
D=A
@3
D=D+A // addr = (5 + 3), into D
@R13
M=D   // Save addr to R13

@SP
M=M-1 // Decrement SP
A=M
D=M   // Read val. from stack into D
@R13
A=M
M=D   // Save val. from stack into addr stored in R13