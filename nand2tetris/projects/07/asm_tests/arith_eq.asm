// Testing translations of arithmetic commands in CPU Emulator
//
// Stack before:      Stack after:
//  | x |              | -1/0? |
//  | y |              |  y    | <- SP
//  |   | <- SP        |       |

// Bootstrap code
@256
D=A
@SP
M=D

// x = 8
@8
D=A
@SP
A=M
M=D
@SP
M=M+1
// y = 7
@7
D=A
@SP
A=M
M=D
@SP
M=M+1

// eq => x-y == 0
@SP
M=M-1
A=M
D=M     // load y
@SP
M=M-1
A=M
D=M-D   // op: x-y -> D
@SP
A=M
M=-1    // true by default
@END_EQ
D;JEQ   // if x-y == 0, jump to END_EQ => accept the default
@SP
A=M
M=0     // false
(END_EQ)
@SP
M=M+1