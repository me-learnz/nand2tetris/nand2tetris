// Testing `pop local i` translation in CPU Emulator

// Bootstrap code
@256
D=A
@SP
M=D

// Init LCL to 20 (example)
@20
D=A
@LCL
M=D

// Save 5 to stack
@5
D=A
@SP
A=M
M=D
@SP
M=M+1

// pop local 2 (put value from stack to Mem[Mem[LCL] + 2])
@LCL
D=M
@2
D=D+A // addr = (Mem[LCL] + 2), into D
@R13
M=D   // Save addr to R13

@SP
M=M-1 // Decrement SP
A=M
D=M   // Read val. from stack into D
@R13
A=M
M=D   // Save val. from stack into addr stored in R13
