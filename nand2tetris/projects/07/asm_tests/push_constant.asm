// Testing `push constant X` translation in CPU Emulator

// Bootstrap code
@256
D=A
@SP
M=D

// push constant 7
@7
D=A
@SP
A=M
M=D
@SP
M=M+1