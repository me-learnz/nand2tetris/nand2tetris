// Specifications of a Guy game


/** Main class */
class Main
    /** Launch the game inside */
    function void main()


/** Base class of the game */
class Game
    field int activeMode;  // Current mode of the game (0 - menu, 1 - map, 2 - exit)
    field Map map;         // Saved map with all positions of objects
    field Menu menu;       // Menu with saved settings

    /** Create a new instance of Game */
    constructor Game new()
        // map = null;
        // menu = Menu.new();
        // activeMode = 0;

    /** Destroy Game and deallocate memory */
    method void dispose()
        // map.dispose()
        // menu.dispose()

    /** Show menu and run a loop waiting for keys */
    method void run()
        // while (~(activeMode == 2)) {
        //     while (key == 0){
        //         key = Keyboard.keyPressed();
        //     }
        //
        //     if activeMode == 0 {        //
        //         response = menu.pressedKey(key);
        //     } else {
        //         response = map.pressedKey(key);
        //     }
        //
        //     _handleResponse(response);
        //
        //     waitUntilKeyIsReleased();
        // }
        //
        // return;

    /** Do stuff based on Map's or Menu's response */
    method void _handleResponse(int response)
        // if activeMode == "menu" {
        //     - new game => map = Map.new(); map.show(); activeMode = map
        //     - resume   => if map { map.show(); activeMode = map }
        //     - exit     => activeMode = 2
        // } else {
        //     - esc => menu.show()
        // }


/** Class for displaying menu and storing settings */
class Menu
    /** New menu with default settings */
    constructor Menu new()

    /** Destroy Menu and deallocate memory */
    method void dispose()

    /** Invoke reaction to a pressed key */
    method int pressedKey(int key)
        // 0) New game
        // 1) Resume
        // 2) Exit

    /** Clear the screen and show the menu */
    method void show()
        // clear screen
        // draw menu (lines)


/** Class for displaying map and handling objects on it */
class Map
    field guy Guy;
    field List enemies;
    field List things;

    /** New map with spawned Guy and a few Creatures */
    constructor Map new()

    /** Destroy Map and deallocate memory */
    method void dispose()

    method int pressedKey(int key)
        // Move up, down, right, left (executes step(pressed_key))
        // 0) Enter menu

    /** Clear the screen and show the map */
    method void show()
        // clear screen
        // refresh()

    /** Refresh objects on map */
    method void _refresh()
        // for x in (guy, enemies, things):
        //     x.redraw()

    /** Execute one step */
    method int _step(int pressed_key)
        // guy.move(pressed_key);
        // for e in enemies:
        //     e.move(guy.x, guy.y)
        // _refresh()
        // _scan_guy_touches()

    /** Scan all things/enemies whether they touch guy */
    method List _scan_guy_touches()
        // for e in enemies:
        //     if guy.bBox.touches(e.bBox):
        //         _encounterEnemy(e);
        //
        // for t in things:
        //     if guy.bBox.touches(t.bBox):
        //         _encounterThing(t);


/** Main character class */
class Guy
    field int x, y;
    field BoundingBox bBox;
    field int speed;         // How many pixels to move per step

    /** New Guy at given coordinates*/
    constructor Guy new(int x, int y)

    /** Destroy Guy and deallocate memory */
    method void dispose()

    /** Move to a desired direction */
    method Array move(int direction)
        // erase()
        // update x, y
        // update bounding box
        // draw()


/** Class for Guy's enemies */
class Enemy
    // Same as Guy


/** Static thing */
class Thing
    // Same as Guy, without moving


/** Bounding box of every object/pawn in game */
class BoundingBox
    field int x, y;           // position of the center
    field int reachX, reachY  // how far it reaches from the center

    /** New BoundingBox */
    constructor BoundingBox new(int x, int y)

    /** Destroy BoundingBox and deallocate memory */
    method void dispose()

    /** Whether it touches another bounding box */
    method boolean touches(BoundingBox box)

    /** Update coordinates */
    method void update(int x, int y)


/** Random number generator */
class Random
    // Later


/** List class provided by the course */
class List
