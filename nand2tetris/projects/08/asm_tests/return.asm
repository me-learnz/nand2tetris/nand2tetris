// Testing `return` translation in CPU Emulator
// Setup is handled by return.tst

@LCL   // FRAME (R13) = LCL
D=M
@R13
M=D

@5      // RET (R14) = *(FRAME-5)
A=D-A
D=M
@R14
M=D

@SP     // pop into *ARG (ARG is set to SP-n-5 from the previous call)
AM=M-1
D=M
@ARG
A=M
M=D

@ARG    // SP = ARG + 1
D=M+1
@SP
M=D

@R13    // THAT = *(FRAME-1)
AM=M-1   // decrementing FRAME in-place
D=M
@THAT
M=D

@R13    // THIS = *(FRAME-2)
AM=M-1
D=M
@THIS
M=D

@R13    // ARG = *(FRAME-3)
AM=M-1
D=M
@ARG
M=D

@R13    // LCL = *(FRAME-4)
AM=M-1
D=M
@LCL
M=D

@R14    // goto RET
A=M
0;JMP