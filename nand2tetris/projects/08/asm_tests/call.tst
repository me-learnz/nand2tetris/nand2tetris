// Testing script for call.asm

load call.asm,

set RAM[0] 317,    // SP
set RAM[1] 317,    // LCL
set RAM[2] 310,    // ARG (previous function called with 2 rgs)
set RAM[3] 3000,   // THIS
set RAM[4] 4000,   // THAT
set RAM[310] 000,  // prev_arg1
set RAM[311] 111,  // prev_arg2
set RAM[312] 1,    // prev return addr
set RAM[313] 2,    // prev LCL
set RAM[314] 3,    // prev ARG
set RAM[315] 4,    // prev THIS
set RAM[316] 5,    // prev THAT
set RAM[317] 77,   // current arg1
set RAM[318] 88,   // current arg2
set RAM[319] 99,   // current arg3
set RAM[0] 320,    // new SP


repeat 300 {
  ticktock;
}