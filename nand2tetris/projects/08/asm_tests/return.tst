// Testing script for return.asm
// Init state is the same as after running call.asm.

load return.asm,

set RAM[0] 326,    // SP
set RAM[1] 325,    // LCL
set RAM[2] 317,    // ARG
set RAM[3] 3000,   // THIS should restore to 3000
set RAM[4] 4000,   // THAT should restore to 4000
set RAM[310] 0,
set RAM[311] 111,
set RAM[312] 1,
set RAM[313] 2,
set RAM[314] 3,
set RAM[315] 4,
set RAM[316] 5,
set RAM[317] 77,
set RAM[318] 88,
set RAM[319] 99,
set RAM[320] 42,   // return address
set RAM[321] 317,
set RAM[322] 310,
set RAM[323] 3000,
set RAM[324] 4000,
set RAM[325] 123,  // result of whatever happened in the function


repeat 300 {
  ticktock;
}