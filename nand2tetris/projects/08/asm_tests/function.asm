// Testing `function xxx k` translation in CPU Emulator

// Bootstrap code
@256
D=A
@SP
M=D

// function bagrfunc 3  (only for k >= 1)
(bagrfunc)
@3
D=A

(bagrfunc_push_local)
@SP
AM=M+1               // increment SP
A=A-1                // save 0 to the previous SP location
M=-1                 // using -1 instead of 0 just to see it in CPU Emulator
@bagrfunc_push_local
D=D-1;JGT            // repeat push if D > 0

// function bagrfunc 0  (only for k == 0)
(bagrfunc)