// Testing `call xxx n` translation in CPU Emulator
// Setup is handled by call.tst

// call bagrfunc 3
@ret_addr_call:0  // push return address to stack
D=A
@SP
AM=M+1
A=A-1
M=D

@LCL      // push LCL to stack
D=M
@SP
AM=M+1
A=A-1
M=D

@ARG      // push ARG to stack
D=M
@SP
AM=M+1
A=A-1
M=D

@THIS      // push THIS to stack
D=M
@SP
AM=M+1
A=A-1
M=D

@THAT      // push THAT to stack
D=M
@SP
AM=M+1
A=A-1
M=D

@SP
D=M
@LCL      // LCL = SP
M=D
@3        // n here
D=D-A
@5
D=D-A
@ARG      // ARG = SP-n-5
M=D

@bagrfunc // goto bagrfunc (shows address 16 since no label was defined)
0;JMP

(ret_addr_call:0)