// Testing my negater

load Neg.hdl,
output-file Neg.out,
compare-to Neg.cmp,
output-list in%B1.16.1 sel%D2.1.2 out%B1.16.1;

set in 0,
set sel 0,
eval,
output;

set sel 1,
eval,
output;

set in %B1001100001110110,
set sel 0,
eval,
output;

set sel 1,
eval,
output;

set in %B1111111111111111,
set sel 0,
eval,
output;

set sel 1,
eval,
output;