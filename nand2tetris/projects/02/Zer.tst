// Testing my zeroer

load Zer.hdl,
output-file Zer.out,
compare-to Zer.cmp,
output-list in%B1.16.1 sel%D2.1.2 out%B1.16.1;

set in 0,
set sel 0,
eval,
output;

set sel 1,
eval,
output;

set in %B1001100001110110,
set sel 0,
eval,
output;

set sel 1,
eval,
output;

set in %B1010101010101010,
set sel 0,
eval,
output;

set sel 1,
eval,
output;