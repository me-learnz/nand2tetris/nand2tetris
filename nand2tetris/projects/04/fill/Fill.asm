// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

(INIT)
    @24575
    D=A
    @addr
    M=D     // Pixel's starting address = 8191 + SCREEN = 512 * 256 - 1 + 16384
(READKEY)   // Detect a pressed key
    @pixel
    M=0     // Init pixel value to white
    @KBD
    D=M
    @BLACKPIXEL
    D;JNE   // Change pixel to black in case of pressed key
(DRAWLOOP)
    @pixel
    D=M
    @addr
    A=M
    M=D     // Memory[addr] = pixel
    @addr
    M=M-1   // addr = addr - 1
    D=M
    @SCREEN
    D=D-A   // Memory[addr] - 16384
    @INIT
    D; JLT
    @DRAWLOOP
    0; JMP    
(BLACKPIXEL)
    @pixel
    M=-1     // Init pixel value to black
    @DRAWLOOP
    0; JMP
    
    
    