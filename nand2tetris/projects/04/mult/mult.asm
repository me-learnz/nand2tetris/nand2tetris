// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Result (R0*R1) can be in the following interval [-32768, 32767).
// Oops! I accidentally implemented it even for R0 and R1 < 0.
// Could be improved by detecting which one of R0 or R1 is shorter and decrement that.


    @sign   // sign of the multiplication (111... - negative, 000... - positive)
    M=0     // assume positive multiplication by default
(TESTR0)    // Test the sign of R0
    @R0
    D=M
    @R0
    D=D+M   // Add D+D: results either in 2*D (positive R0), -2*D (negative R0) or 0 (R0 = 0)
    @INVR0
    D; JLT  // In case of negative R0, jump to INVR0
(TESTR1)    // Test the sign of R1 (same principle as TESTR0)
    @R1
    D=M
    @R1
    D=D+M
    @INVR1
    D; JLT
(PROLOGUE)  // Initialize the main loop
    @R2     // Init result to 0
    M=0
(LOOP)      // Add R0 to R2 R1-times; R2 is initialized to 0
    @R1
    D=M
    @EPILOGUE
    D; JLE  // If R1 <= 0, jump to EPILOGUE
    @R0
    D=M
    @R2
    M=D+M   // R2 = R0 + R2
    @R1
    M=M-1   // R1 = R1 - 1
    @LOOP
    0; JMP
(EPILOGUE)  // Revert (or not) the result based on the sign variable
    @sign
    D=M
    @INVRES
    D; JNE  // If sign is non-zero, invert the sign of the result
(END)       // Endless loop at the end
    @END
    0; JMP
(INVRES)
    @R2
    M=-M
    @END
    0; JMP    
(INVR0)
    @sign
    M=!M     // invert sign
    @R0
    M=-M     // R0 -> -R0
    @TESTR1
    0; JMP
(INVR1)
    @sign
    M=!M    
    @R1
    M=-M
    @PROLOGUE
    0; JMP    