# From NAND to Tetris

## Projects

| Num | Project name                     | Status | External repo |
|:---:| -------------------------------- | ------ | ------------- |
|  01 | **Boolean Logic**                | [![Project 01](https://www.dropbox.com/s/rvctgze8uj7gaiv/nand2tetris-master-test_01.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |
|  02 | **Boolean Arithmetic**           | [![Project 02](https://www.dropbox.com/s/pqzbzy50dimjb50/nand2tetris-master-test_02.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |
|  03 | **Sequential Logic**             | [![Project 03](https://www.dropbox.com/s/x05r7smjhot9feo/nand2tetris-master-test_03.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |
|  04 | **Machine Language**             | [![Project 04](https://www.dropbox.com/s/v36xkpnuyqpih3d/nand2tetris-master-test_04.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |
|  05 | **Computer Architecture**        | [![Project 05](https://www.dropbox.com/s/e98b8ihhuzhq53o/nand2tetris-master-test_05.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |
|  06 | **Assembler**                    | [![Project 06](https://www.dropbox.com/s/6gskkhne1d0h9z7/nand2tetris-master-test_06.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | [hackass](https://gitlab.com/me-learnz/nand2tetris/hackass) |
|  07 | **VM I: Stack Arithmetic**       | [![Project 07](https://www.dropbox.com/s/z62qv5oqb5jxz0m/nand2tetris-master-test_07.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | [vmhack](https://gitlab.com/me-learnz/nand2tetris/vmhack)   |
|  08 | **VM II: Program Control**       | [![Project 08](https://www.dropbox.com/s/rjatjjd5a6jp4jw/nand2tetris-master-test_08.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | [vmhack](https://gitlab.com/me-learnz/nand2tetris/vmhack)   |
|  09 | **High-Level Language**          | [![Project 09](https://www.dropbox.com/s/ssaz5ohzfyiok59/nand2tetris-master-test_09.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |
|  10 | **Compiler I: Syntax Analysis**  | [![Project 10](https://www.dropbox.com/s/yyjsevox2qqz16w/nand2tetris-master-test_10.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | [jackc](https://gitlab.com/me-learnz/nand2tetris/jackc) |
|  11 | **Compiler II: Code Generation** | [![Project 11](https://www.dropbox.com/s/yanr50ruonubio2/nand2tetris-master-test_11.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |
|  12 | **Operating System**             | [![Project 12](https://www.dropbox.com/s/1lqxt38jvlylaoa/nand2tetris-master-test_12.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |
|  13 | **More Fun to Go**               | [![Project 13](https://www.dropbox.com/s/85obpuyp76esmpe/nand2tetris-master-test_13.svg?raw=1)](https://gitlab.com/me-learnz/nand2tetris/nand2tetris/pipelines) | - |

## Branches

* [**master**](https://gitlab.com/me-learnz/nand2tetris/tree/master): Main working branch. All solutions go here.
* [**clean**](https://gitlab.com/me-learnz/nand2tetris/tree/clean): Clean state without any contributions. Contains untouched software bundle and project files.

## Links

* http://www.nand2tetris.org/
* http://www1.idc.ac.il/tecs/plan.html
